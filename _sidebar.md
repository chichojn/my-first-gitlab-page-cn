<!-- docs/_sidebar.md -->

* Guides
  * [Get Start](/)
* Plugin
  * [Shopify](/plugin/shopify/index)
  * [Woocommerce](/plugin/woocommerce/index)
* Payment Methods
	* [支付宝线上](/payment-methods/alipay-online/index)
	* [SDK（支付宝线上）](/payment-methods/sdk-alipay-online/index)
	* [支付宝扫码](/payment-methods/alipay-qr/index)
	* [微信支付](/payment-methods/wechat/index)
	* [泰国二维码](/payment-methods/thai-qr/index)
	* [银行卡](/payment-methods/bankcard/index)
* Reference
  * [Currency](/reference/currency)
  * [Order status](/reference/order-status)
  * [Refund status](/reference/refund-status)
  * [Return Code](/reference/return-code)
