# Getting Started

<a name="OverView" id="overview"></a>
## 1. 文档介绍

<a name="DocPurpose" id="docpurpose"></a>
### 1.1 文档目的

>本文档由连连电子支付（泰国）有限公司（以下简称“连连泰国”）提供。为接入连连泰国支付宝扫码产品的商户和平台提供技术对接指导。文档的目标读者为商户或平台的技术人员。

<a name="EnvCon" id="envcon"></a>
### 1.2 环境配置
>商户开始对接前连连泰国会提供以下信息至商户，同时商户也需提供自己的公钥和IP信息至连连泰国进行配置 。以下参数信息测试环境和生产环境不同。

|Name| Description |Remark |
| --- | --- |  --- | 
| merchant_id  | I连连泰国为商户分配的ID||
| public_key  | 公钥 ||
| store_id  | 连连泰国为商户下属门店的ID |for in-store merchants only|

<a name="TechSup" id="techsup"></a>
### 1.3 技术支持
| Name | email | 
| --- | --- | 
| IT support  | support@lianlianpay.co.th|

<a name="SpecSecure" id="specsecure"></a>
## 2. 报文规范及安全

<a name="MessEndcode" id="messendcode"></a>
### 2.1 报文编码及结构

>连连泰国的API通信统一采用HTTPS的POST/GET方式，POST请求类型为application/json ，UTF-8编码。请参考下图报文格式的示例。

<a name="Http" id="http"></a>
#### 2.1.1 HTTP头

商户至连连

|<div style="width:90px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:210px">Description</div>|
| --- | --- |---| --- |
| sign-type| string(3)| <center>N</center>| 数字签名类型固定：RSA |
| sign| string(128) |<center>Y</center>| 数字签名 |
| Content-Type| string(32)| <center>N </center>|HTTP POST时，application/json|

连连至商户

|<div style="width:90px">Field</div>| <div style="width:90px">Type</div> |<div style="width:50px">Required</div>|<div style="width:210px">Description</div>|
| --- | --- |---| --- |
| sign-type| string(3)|<center>N</center>| 数字签名类型固定：RSA |
| sign| string(128) |<center>N</center>| 数字签名 |

<a name="GenResMess" id="genresmess"></a>
#### 2.1.2 通用响应信息

|<div style="width:90px">Field</div>| <div style="width:90px">Type</div> |<div style="width:50px">Required</div>|<div style="width:210px">Description</div>|
| --- | --- |---| --- |
| code| string(6)|<center>Y</center>| [参考附录 3.4](#ReturnCode) |
| message | string(128) |<center>N</center>| [参考附录 3.4](#ReturnCode) |
| data | object |<center>N</center>| 业务数据 |
| trace_id| string(32)| <center>Y</center>|交易追踪|

<a name="DigitSign" id="digitsign"></a>
### 2.2 数字签名 
>通信报文需要有RSA数字签名，HTTP HEADER不参与签名，返回报文只有成功时的data数据参与签名。

<a name="RequestSign" id="requestsign"></a>
#### 2.2.1 请求报文签名
>GET请求的加签范围为: Query，POST请求的加签范围为Body。
- Query: 按照字典升序以key1=value1& key2=value2形式排序，记为Y
- Body: 按照key字典升序以key1=value1& key2=value2形式排序，当value类型为   JSON时，按照该JSON的key继续升序排序，结果追加到排序串，当value类型为JSON array时，逐个获取元素，再按照元素的key进行排序，记为Z

GET请求最终加签字符串提取为：Y；POST请求提取为Z。然后使用算法SHA1withRSA加签。

Body提取加签串例子如下：
```
{
	"c":{"b":"11","a":"10"},
	"a":"100",
	"b":[{"c":"3","b":"2","a":"1"},{"c":"6","b":"5","a":"4"}]
}
```
<b>第一步</b>、按照最外层 c、a、b排序升序排列，得到：
<ul><ul>1. "a":"100" </ul></ul>
<ul><ul>2. "b":[{"c":"3","b":"2","a":"1"},{"c":"6","b":"5","a":"4"}]</ul></ul>
<ul><ul>3. "c":{"b":"11","a":"10"}</ul></ul>

<b>第二步</b>、拿1（a）得到当前排序串为 a=100，<br>
<b>第三步</b>、拿2（b）类型为arrayjson，
			  则获取数组第一个元素{"c":"3","b":"2","a":"1"}，进行c、b、a升序排列为a、b、c得到的当前<code>排序串</code>为a=100&a=1&b=2&c=3，<br>
			  继续获取第二个元素{"b":"5","c":"6","a":"4"}，进行b、c、a升序排列为a、b、c得到的<code>排序串</code>为a=100&a=1&b=2&c=3&a=4&b=5&c=6<br>
<b>第四步</b>、拿3（c）时，类型为json，根据key升序排序，得到最终的<code>排序串</code>为：
```
a=100&a=1&b=2&c=3&a=4&b=5&c=6&a=10&b=11
```
<a name="ResponseSign" id="responsesign"></a>
#### 2.2.2 响应报文签名 
>交易失败时不加签，交易成功时，若data不空，则对data加签，加签方式参考上面示例。

<a name="DataTypeEx" id="datatypeex"></a>
### 2.3 类型说明
|Data Type Syntax | Description |
| --- | --- |
| string(p,s)| 金额字符串，p,s均为正整数，p表示精度s为小数位|
| string(n)  | n为正整数表示字符串最大长度 |