# 连连泰国支付SDK（支付宝线上）接入手册



## 1. 集成方式 
> 连连泰国提供SDK集成方式。

### 1.1. SDK
<div align="center">
<img width="600" src="./images/alipay/sdk.PNG"/>
</div>

## 2. 对象定义 
<a name="Product" id="product"></a>
### 2.1. Product
|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:350px">Description</div>|
| --- | --- |--- |--- |
| name  | string(128)       |<center> Y </center>| 商品名称  | 
| description| string(128)  |<center> N</center>|商品描述，不允许包含”’等特殊字符 | 
| unit_price| string(15,2)  |<center> Y</center>| 商品单价，需保留两位小数 Eg.100.00 | 
| quantity| string(10)      |<center> Y</center>    |商品数量，正整数 | 
| category| string (64)     |<center> N</center>| 商品分类 | 
| show_url| string(256)     |<center> N</center>    | 商品网址| 

<a name="Address" id="address"></a>
### 2.2. Address 
|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:350px">Description</div>|
| --- | --- |--- |--- |
|street_name    |string(128)|<center>N</center> |客户街道名称|
|house_number   |string(32) |<center>N</center> |客户街道门牌号|
|district   |string(32)     |<center>N</center> |区|
|city   |string(64)         |<center>N</center> |城市|
|state  |string(2)          |<center>N</center> |省份，缩写。 |
|country    |string(2)      |<center>N</center> |国家缩写  |
|postal_code    |string(16) |<center>N</center> |邮编 |

<a name="Customer" id="customer"></a>
### 2.3. Customer 
|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:350px">Description</div>|
| --- | --- |--- |--- |
|merchant_user_id|  string(64)  |<center>Y</center> |买家在商户中的唯一标志|
|customer_type  |string(1)      |<center>N</center> |客户身份类型，目前仅支持个人<br>I =Individual （个人）<br>C=Corporation（公司）|
|first_name |string(64)         |<center>N</center> |Only for individual|
|last_name  |string(64)         |<center>N</center> |Only for individual|
|full_name  |string(128)        |<center>Y</center> |个人全名或者公司名称|
|gender |string(16)             |<center>N</center> |性别：MALE, FEMALE, UNKNOWN|
|id_type    |string(16)         |<center>N</center> |证件类型|
|id_no  |string(32)             |<center>N</center> |证件号|
|email  |string(64)             |<center>N</center> |客户邮件地址|
|phone  |string(32)             |<center>N</center> |格式：”+国家或者区号-手机号”|
|company    |string(128)        |<center>N</center> |个人公司名称|
|address    |Address            |<center>N</center> |地址信息, [详见 2.2](#Address)|

## 3. SDK接入 
> 移动支付交易SDK创建接口技术文档面向具有一定Android/iOS客户端开发能力，了解Android/iOS客户端的开发和管理人员。

### 3.1. SDK交互流程 
<div align="center">
<img width="600" src="./images/alipay/sdk-interaction-process.PNG"/>
</div>

> 流程说明（以Android平台为例）：

1.  第4步：调用支付接口：此消息就是本接口所描述的开发包提供的支付对象LianPayManager，将商户签名后的订单信息传进pay方法唤起或跳转支付宝/微信收银台，订单格式具体参见<b>“SDK支付请求参数说明 ”</b>。 

2.   第5步：跳转支付宝或微信进行支付：连连Android/iOS支付开发包将会按照商户客户端提供的请求参数发送支付请求，成功后便会唤起或跳转支付宝收银台以进行支付确认。 

3.  第8步：接口返回支付结果：商户客户端在第4步中调用的支付接口，会返回最终的支付结果（即同步通知），参见
<b>“SDK支付同步返回参数说明”</b>。 

4.   第12步：异步发送支付通知：手机支付宝支付服务器端发送异步通知消息给商户服务器端（备注：第12步一定发生在第6步之后，但不一定晚于7-11步），参见<b>“支付结果通知参数说明”</b>。 


### 3.2. SDK支付请求 

#### 3.2.1. SDK支付请求参数说明 
> SDK payment request parameter description
<b>请求参数列表:</b>

|<div style="width:130px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- |--- |--- |
| merchant_id	| String(20)	|<center>Y</center>| 商户ID| 
| merchant_order_id	| string(64)|<center>Y</center>	| 商户交易ID| 
| biz_id	| string(20)	    |<center>Y</center>	| 业务ID| 
| order_amount	| string(8, 2)	|<center>Y</center>| 订单金额| 
| order_currency	| string(3)	|<center>Y</center>| 币种| 
| order_info	| string(256)	|<center>Y</center>| 向买家展示的信息| 
| product_code	| string(64)	|<center>Y</center>| 产品编码: ALIPAY_ONLINE| 
| payment_method	| string(32)|<center>Y</center>| 支付方式: APP_PAYMENT| 
| customer	| Customer          |<center>N</center>| [客户信息详见 2.3](#Customer) | 
| products	| List Product      |<center>N</center>| [商品信息](#Product)| 
| notify_url	| string(256)	|<center>N</center>| 支付结果通知地址| 
| redirect_url	| string(256)	|<center>N</center>| 支付成功后，用户页面回跳URL地址| 
| sign_type	| string(3)	        |<center>N</center>| RSA| 
| sign	| string(128)	        |<center>Y</center>| 商户签名  | 

#### 3.2.2. SDK支付同步返回参数说明 
> 返回参数列表:

|<div style="width:130px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- |--- |--- |
| merchant_id	| String(20)  |<center>Y</center>| 商户ID| 
|order_id	|string(20）      |<center>Y</center>|支付单ID|
|order_amount	|string(8, 2) |<center>Y</center>|订单金额|
|order_currency	|string(3)	  |<center>Y</center>|币种|
|order_status	|string(8)	  |<center>Y</center>|订单状态|
|sign	|tring(256)	          |<center>Y</center>|签名|

#### 3.2.3. SDK返回码说明 
> PayResult结果码含义如下:

|SDK status | Message |
| --- | --- |
|1000	|	支付成功|
|1010	|	订单正在处理中|
|1020	|	已支付|
|2000	|	配置参数错误|
|2001	|	SDK内部错误|
|2002	|	SDK不可用|
|2003	|	支付方式错误|
|3000	|	参数错误|
|3001	|	签名参数错误|
|3010	|	网络连接错误|
|4000	|	支付失败|
|5000	|	用户中途取消|
|5005	|	支付结果未知|
|9999	|	服务繁忙|

### 3.3. App申请 
> 开发者在接入SDK前首先需要完成应用创建，登陆商户站，在左侧菜单栏选择，点击创建应用：

<div align="center">
<img width="600" src="./images/alipay/add-app.PNG"/>
</div>
<b>必填项包括:</b>

1.  应用名称 
2.  应用类型，支持Android、iOS、Web（暂未提供相应的SDK） 
3.  应用描述  
4.  应用图标  
5.  包名/BundleId/域名（如果是Android应用，则输入包名；如果是iOS应用，则输入Bundle Id；如果是Web应用，则输入域名，与Web部署站点域名必须一致）

选填项包括:

1.  APP签名摘要：只适用于Android平台，用于验证APP有效性，支持SHA256、SHA1、MD5，商户站APP签名摘要配置与SDK前端签名摘要类型配置（默认SHA256）务必保持一致
2.  网站地址

注意：必填项在审核通过后不能修改，选填项在审核通过后仍可自由修改，APP签名摘要配置不当会导致SDK不可用，务必配置正确

APP申请完成后提交管理员审核，审核通过后，会生成App Key，开发者需将这个App Key配置到SDK参数中，详见Android及iOS集成步骤。

### 3.4. Android集成步骤 

#### 3.4.1. SDK集成导入 
> （一）导入开发资源 

1. 将 AlipaySDK-<版本>.aar 和 LLPayThaiSDK-<版本>.aar 包放入商户应用工程的libs目录下
2. 在app module中添加依赖Gradle配置

```
{
	repositories {
    flatDir {
        dirs 'lib'
    }
}
dependencies {
    implementation (name: 'AlipaySDK-15.7.7', ext: 'aar')
    implementation (name: 'LLPayThaiSDK-1.0.0', ext: 'aar')
}

}
```

>（二）添加权限声明 

在商户应用工程的AndroidManifest.xml中添加权限配置：

```
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
```

> （三）添加混淆规则 Add rules of confusing

在商户应用工程的`proguard-project.txt`里添加以下相关规则：

```
-keep class th.co.lianlianpay.sdk.core.**{ *; }
-keep class th.co.lianlianpay.sdk.constants.**{ *; }
-keep class th.co.lianlianpay.sdk.model.**{ *; }
-keep class th.co.lianlianpay.sdk.callback.**{ *; }
-keep class th.co.lianlianpay.sdk.widget.**{ *; }
-keep class th.co.lianlianpay.sdk.task.**{ *; }
-keep class th.co.lianlianpay.sdk.enums.**{ *; }
 
-keep class com.alipay.android.app.IAlixPay{*;}
-keep class com.alipay.android.app.IAlixPay$Stub{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback$Stub{*;}
-keep class com.alipay.sdk.app.PayTask{ public *;}
-keep class com.alipay.sdk.app.AuthTask{ public *;}
```

至此，开发包开发资源导入完成。

#### 3.4.2. SDK使用步骤 

>（一）配置商户APP KEY 

可以在AndroidManifest.xml中配置，也可以通过API进行设置
```
<!--连连支付SDK-->
<meta-data android:value="C3UBLN6225orgreopddj1" android:name="LIANPAY_APPKEY"/>
```

>（二）SDK初始化 

建议在Application中进行初始化
```
public class AppApplication extends Application {
 
    @Override
    public void onCreate() {
        super.onCreate();
        LianPaySDK.init(this).setSandbox(true).setDebug(true);
    }
}
```

参数说明：

sandbox：为true则可在连连及支付宝沙箱环境下进行调试，默认为false，需在连连及支付宝生产环境下使用<br>
debug：是否调试状态，调试状态下日志将会打开<br>
appKey：手动设置appKey<br>
digestType：APP签名摘要类型，必须与开放平台配置摘要类型一致，可设值为SHA256、SHA1、MD5，默认为SHA256<br>

>（三）SDK使用 Use a SDK

SDK支持API方式和组件方式实现支付操作，两者传参均为PayRequest和PayCallback，PayRequest是支付订单信息参数，PayCallback是支付结果回调接口。

（1）构建支付订单参数 
```
PayRequest payRequest = new PayRequest();
payRequest.setMerchantId("142019050800009001");  //商户号
payRequest.setBizId("142019111200055026");  //业务号
payRequest.setMerchantOrderId("P" + System.currentTimeMillis());  //商户订单号
payRequest.setOrderCurrency("THB");  //币种
payRequest.setOrderAmount("101.25");  //支付金额
payRequest.setOrderInfo("app payment");  //订单描述信息
payRequest.setNotifyUrl("http://www.yezhou.cc/callback/notify.php");  //支付完成异步通知地址
payRequest.setRedirectUrl("http://www.yezhou.cc/callback/redirect.php");  //支付完成重定向地址  
payRequest.setPayType(PayType.AliPay);  //与下面两句ProductCode和PaymentMethod设置等效
//payRequest.setProductCode(ProductCode.ALIPAY_ONLINE.name());
//payRequest.setPaymentMethod(PaymentMethod.APP_PAYMENT.name());
 
Customer customer = new Customer();
customer.setFirstName("Joe");
customer.setLastName("Ye");
customer.setEmail("yezhou@yezhou.org");
customer.setPhone("+8610086");
payRequest.setCustomer(customer);
 
//获取加签串（可直接提交商户服务端进行加签
String preStr = PayHelper.buildSignString(payRequest);
Log.i(LianConstants.TAG, "preStr: " + preStr);
 
//模拟计算商户签名（务必放在商户服务端加签
String sign = SignUtil.addSign(payRequest, merchantPrivateKey);
Log.i(LianConstants.TAG, "sign: " + sign);
payRequest.setSignType(SignType.RSA.name());
payRequest.setSign(sign);
 
//获取请求体
String body = PayHelper.formatPayRequest(payRequest);
Log.i(LianConstants.TAG, "body: " + body);
```
（2）API方式进行支付
```
LianPayManage lianPayManage = new LianPayManage(MainActivity.this);
lianPayManage.pay(buildPayRequest(), payCallback);
```
（3）组件方式进行支付 
```
<th.co.lianlianpay.sdk.widget.LianPayWidget
    android:id="@+id/layout_lian_pay"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:layout_constraintTop_toBottomOf="@+id/ll_order"
    app:layout_constraintLeft_toLeftOf="parent"
    app:layout_constraintRight_toRightOf="parent"
    android:layout_marginTop="20dp"
    android:layout_marginLeft="10dp"
    android:layout_marginRight="10dp"
    android:background="@drawable/shape_layout_round_rect_bg"
    />
```
```
LianPayWidget lianPayWidget = findViewById(R.id.layout_lian_pay);
lianPayWidget.withAliPay(true).withWechatPay(true).defaultPayType(LianPayWidget.ALI_PAY);
        //.setRadioBackground(R.drawable.selector_checkbox_bg);
 
lianPayWidget.startPay(buildPayRequest(), payCallback);
```
（4）支付结果回调处理
PayCallback回调接口包含三个方法：
```
void onPayResult(PayResult payResult)：支付动作结束，主要进行支付结果处理
void onPayStart()：支付动作开始，可以做一些初始化工作，如加载进度条等
void onPayPrepared(PayType payType)：支付准备完成回调，下一步即将唤起或跳转支付宝/微信
```
支付回调处理示例：
```
private PayCallback payCallback = new PayCallback() {
    @Override
    public void onPayResult(PayResult payResult) {
        Log.i(TAG, JSON.toJSONString(payResult));
        LoadingDialog.dismiss(MainActivity.this);
        if (LianSdkResult.PAY_SUCCESS.getStatus().equals(payResult.getStatus())) {
            Toast.makeText(MainActivity.this, "支付成功", Toast.LENGTH_SHORT).show();
            //模拟商户验签（建议放在商户服务端验签
            Map<String, Object> result = payResult.getResult();
            String sign = (String) result.get("sign");
            result.remove("sign");
            boolean checkSignResult = SignUtil.checkSign(sign, result, lianlianPublicKey);
            if (checkSignResult) {
                Toast.makeText(MainActivity.this, "验签成功", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "验签失败", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(MainActivity.this, payResult.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
 
    @Override
    public void onPayStart() {
        LoadingDialog.show(MainActivity.this, "Paying...");
    }
 
    @Override
    public void onPayPrepared(PayType payType) {
        LoadingDialog.dismiss(MainActivity.this);
    }
};
```
>（四）支付效果 
参考Demo，支付流程演示效果如下:

<div align="center">
<img width="600" src="./images/alipay/payment-result.PNG"/>
</div>

### 3.5. iOS集成步骤 

#### 3.5.1. 获取SDK 
1.  下载连连泰国移动支付iOS版本的SDK：LLPayThaiSDK-iOS-v1.0.0.zip
2.  解压之后，将会获得用于集成的LLPayThaiSDK.framework，如下图：
<div align="center">
<img width="600" src="./images/alipay/sdk-access-sdk.PNG"/>
</div>

#### 3.5.2. 导入并配置SDK 
1.  启动Xcode，将刚刚获取到的LLPayThaiSDK.framework拷贝到您的项目文件夹下，如下图：
<div align="center">
<img width="600" src="./images/alipay/sdk-import-config.PNG"/>
</div>

2.  导入到项目工程中，如下图：
<div align="center">
<img width="400" src="./images/alipay/import-into-project.PNG"/>
</div>

3.  在Build Phases选项卡的Link Binary With Libraries中，增加以下依赖：
<div align="center">
<img width="600" src="./images/alipay/add-dependence.PNG"/>
</div>
其中，需要注意：<br>
(1) 如果是Xcode 7.0之后的版本，需要添加libc++.tbd、libz.tbd；<br>
(2) 如果是Xcode 7.0之前的版本，需要添加libc++.dylib、libz.dylib（如下图）。
<div align="center">
<img width="600" src="./images/alipay/please-note.PNG"/>
</div>

4.  点击项目名称，点击“Info”选项卡，在“URL Types”选项中，点击“+”，在“URL Schemes”中输入“LLPayThaiSDKDemo”，如下图：
<div align="center">
<img width="600" src="./images/alipay/click-project-name.PNG"/>
</div>
注意：如果不完成此配置，用户在完成支付后，将无法回跳到商户App完成整个支付流程

#### 3.5.3. 使用SDK，可参考SDK使用
1.  在AppDelegate.m文件中，增加头文件引用，并添加核心代码（想要查看更多代码，可获取DEMO自行查看），如下：

```
#import <LLPayThaiSDK/LLPayThaiSDK.h>
  
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.window.rootViewController = self.vc;
    [self.window makeKeyAndVisible];
     
    //AppKey is obtained by the merchant registering the App at the merchant portal
    [[LLPayThaiSDK sharedInstance] registerApp:@"C3UBLN6225orgreopddj" productEnvironment:NO];
     
    return YES;
}
 
 
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
     
    if ([url.host isEqualToString:@"safepay"]) {
        //Jump to Alipay wallet for payment and process payment results
        [[LLPayThaiSDK sharedInstance] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            self.vc.payButton.enabled = YES;
             
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Payment Result" message:resultDic[@"message"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                 
            }]];
             
            [self.vc presentViewController:alert animated:YES completion:NULL];
        }];
    }
    return YES;
}
 
// NOTE: Use the new API interface after 9.0
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
    if ([url.host isEqualToString:@"safepay"]) {
        //Jump to Alipay wallet for payment and process payment results
        [[LLPayThaiSDK sharedInstance] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@", resultDic);
            self.vc.payButton.enabled = YES;
             
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Payment Result" message:resultDic[@"message"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                 
            }]];
             
            [self.vc presentViewController:alert animated:YES completion:NULL];
        }];
    }
    return YES;
}
```

2.  在您想要发起支付的文件中，增加头文件引用，并添加核心代码（想要查看更多代码，可获取DEMO自行查看），如下：

```
#import <LLPayThaiSDK/LLPayThaiSDK.h>
 
 
- (void)pay:(id)sender {
     
    self.payButton.enabled = NO;
    // 商户自己的私钥，用于加签业务参数 Merchant’s private key, used to sign business parameters
    NSString *privateKey = @"xxxxxx";
     
    // SDK支付请求对象  SDK payment request object
    LLPayThaiPaymentObject *payObject = [[LLPayThaiPaymentObject alloc] init];
    payObject.merchantId = @"142019050800009001";   // 商户在连连的商户ID
    payObject.bizId = @"142019111200055026";    // 商户在连连的业务ID
    payObject.channelCode = ChannelCodeAlipay;  //  选择的支付方式，目前仅支持支付宝
    payObject.productCode = @"ALIPAY_ONLINE";   // 支付产品，支付宝：alipay_online Payment product, Alipay: Alipay_online
    payObject.paymentMethod = @"APP_PAYMENT";   // 支付方式，固定值：APP_PAYMENT
    payObject.merchantOrderId = [self generateTradeNO]; // 传给连连的商户订单号，可由商户自定生成规则
    payObject.orderCurrency = @"THB";   // 支付币种，目前仅支付THB
    payObject.orderAmount = @"0.05";    // 支付金额，保留小数后两位
    payObject.orderInfo = @"sdk demo test order info";  // 商户支付的订单信息
    payObject.memo = @"test order"; // 商户支付单备注信息
    payObject.notifyUrl = @"https://www.baidu.com"; // 商户服务端通知地址，用于接收支付成功通知
     
    // 生成加签字符串 Generate signature string
    ModelUtil *modelUtil = [[ModelUtil alloc] init];
    NSDictionary *payDict = [modelUtil dicFromObject:payObject];
    NSString *signString = [modelUtil generateSignString:payDict excludeFields:nil];
     
    // 用私钥加签，生成业务参数签名 Sign with private key to generate business parameter signature
    NSString *sign = [CryptoUtil rsaSignString:[NSString stringWithFormat:@"%@", signString] WithPrivateKey:privateKey];
    payObject.sign = sign;
     
    // 发起支付 Initiate payment
    [[LLPayThaiSDK sharedInstance] payOrder:payObject fromScheme:@"LLPayThaiSDKDemo" callback:^(NSDictionary *resultDic) {
        // 支付结果处理 Payment result handling
        self.payButton.enabled = YES;
         
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Payment Result" message:resultDic[@"message"] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
             
        }]];
         
        [self presentViewController:alert animated:YES completion:NULL];
    }];
}
```
注意：在DEMO中，提供一些工具类，这些文件是为示例签名所在客户端本地使用。出于安全考虑，请商户尽量把私钥保存在服务端，在服务端进行签名验签。

3.  发起支付申请，如果申请成功，将会跳转到支付宝App收银台，要求用户确认支付，如下图：
<div align="center">
<img width="300" src="./images/alipay/initiate-payment.PNG"/>
</div>

4.  用户完成支付后，会回跳到商户App，由商户展示支付结果