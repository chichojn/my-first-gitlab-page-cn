# 连连泰国支付网关（微信支付）接入手册

## 1. 集成方式 
>连连泰国提供 API 集成方式。

#### 1.1. 交互流程  
<div align="center">
<img width="700" src="./images/wechat/interactiveFlow.PNG"/>
</div>

## 2. 接口定义  
### 2.1. 支付 
>商户订单ID需要保证唯一，不允许发起重复提交。

#### 2.1.1. 支付请求 
>请求参数列表:

|<div style="width:122px" align="left"> 沙箱 </div>|<div style="width:545px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>| 
| --- | --- | 
| <b>生产URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式</b> | POST|

|<div style="width:110px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- | --- | --- | 
| version | string(2) 		|<center>Y</center>| 接口版本号，固定值：v1 | 
| service| string(64) 		|<center>Y</center>| 接口名称，固定值：llpth.wechatpay.pay| 
| merchant_id | string(20) 	|<center>Y</center>| 商户ID  | 
|merchant_order_id |string(64）|	<center>Y</center>|商户交易ID  | 
|order_amount |	string(8, 2)|<center>Y</center>| 订单金额  | 
|order_currency	|string(3)	|<center>Y</center>| 币种| 
|order_info|string(128)		|<center>Y</center>|向买家展示的信息| 
|store_id| string(20) 		|<center>N</center>|门店ID，门店的唯一标识| 
|payment_method |	string(32)	|<center>Y</center>| 支付方式：DYNAMIC_CODE,<br> INAPP_PAYMENT（公众号或小程序）,<br> MERCHANT_SCAN；| 
|notify_url | string(256) 		|<center>N</center>| 支付结果通知地址| 
|auth_code | string(20)		|<center>C</center> | 用户支付条码，payment_method为MERCHANT_SCAN时必传| 
|appid | string(20)			|<center>C</center>| 商户公众号或小程序appid，<br>payment_method为<br>INAPP_PAYMENT时必传.| 
|openid | string(32) 		|<center>C</center>| 公众号或小程序用户openid，<br>payment_method为 <br>INAPP_PAYMENT时必传| 

<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess) 

| Field  | Type| Required  | Description|
| --- | --- | --- | --- | 
| order_id	| string(20)		|<center>Y</center>| 支付单ID| 
| order_status	| string(8)		|<center>Y</center>| 	订单状态| 
| order_amount	| string (8,2)	|<center>Y</center>| 	订单金额| 
| order_currency| 	string (3)	|<center>Y</center>| 	币种| 
| create_time| 	string (19)		|<center>Y</center>| 	订单时间| 
| link_url| string (32)			|<center>N</center>| payment_method为DYNAMIC_CODE时返回订单码的二维码字符串| 
| pay_params| 	text			|<center>N</center>| payment_method为INAPP_PAYMENT时返回公众号或小程序支付参数对象| 

<b>动态码请求样例:</b>
```
{
	"version": "v1",
    "service": "llpth.wechatpay.pay",
    "merchant_id": "142019050800009001",
    "merchant_order_id": "test100072",
    "order_amount": "0.15",
    "order_currency": "THB",
    "order_info": "test",
    "payment_method": "DYNAMIC_CODE",
    "notify_url": "https://www.lianlianpay.co.th/callback"
}
```

<b>动态码返回样例:</b>
```
{
	"code": 200000,
	"data": {
		"order_id": "122020120313403001",
		"order_status": "WP",
		"order_amount": "0.15",
		"order_currency": "THB",
		"create_time": "2020-12-03 10:01:42",
		"link_url": "weixin://wxpay/bizpayurl?pr=kl6sj1g00"
	}
	"message": "Success",
	"trace_id": "54e2983e66b738e6"
}
```

<b>APP内支付请求样例：</b>
```
{
	"version": "v1",
	"service": "llpth.wechatpay.pay",
    "merchant_id": "142019050800009001",
    "merchant_order_id": "test100088",
    "order_amount": "50.8",
    "order_currency": "THB",
    "order_info": "test",
    "payment_method": "INAPP_PAYMENT",
    "notify_url": "https://www.lianlianpay.co.th/callback",
    "appid": "wx26d9f58adc1e2f7e",
    "openid": "oaJiE1PwlwP_Tee0Z7uzrQFJeRqE"
}
```

<b>APP内支付返回样例：</b>
```
{
	"code": 200000,
	"data": {
	"order_id": "122020120913407038",
	"order_status": "WP",
	"order_amount": "50.80",
	"order_currency": "THB",
	"create_time": "2020-12-09 16:10:38",
	"pay_params": {
		"timeStamp": "1607505096",
		"package": "prepay_id=wx091711368479544bc68157c5d6578e0000",
		"paySign": "609ca496c49713b944a4f61f6ca6ed75f45273459767c2dcb42392b4cf51dbc7",
		"appId": "wx26d9f58adc1e2f7e",
		"signType": "HMAC-SHA256",
		"nonceStr": "20201209171136824"
		}
	},
	"message": "Success",
	"trace_id": "54e2983e66b738e6"
}
```

#### 2.1.2. 支付取消 
<b>R请求参数列表:</b>

|<div style="width:122px" align="left"> 沙箱 </div>|<div style="width:534px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>| 
| --- | --- | 
| <b>生产URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式 </b> | POST|

|<div style="width:110px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- | --- | --- | 
| version | string(2) 		|<center>Y</center>| 接口版本号，固定值：v1 | 
| service| string(64) 		|<center>Y</center>| 接口名称，固定值：llpth.wechatpay.pay.cancel| 
| merchant_id | string(20) 	|<center>Y</center>| 商户ID| 
|merchant_order_id |string(64）|<center>Y</center>|商户交易ID  | 

<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess) 

|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- | --- | --- | 
| merchant_id| string(20)		|<center>Y</center>| 支付单ID|
| merchant_order_id	| string(64)|<center>Y</center>| 商户订单ID|
| order_id	| string(20)		|<center>Y</center>| 支付单ID| 
| order_status	| string(8)		|<center>Y</center>| 	订单状态| 
| order_amount	| string (8,2)	|<center>Y</center>| 	订单金额| 
| order_currency| 	string (3)	|<center>Y</center>| 	币种| 
| create_time| 	string (19)		|<center>Y</center>| 	订单时间| 

<b>请求样例:</b>
```
{
	"version": "v1",
	"service": "llpth.wechatpay.pay.cancel",
    "merchant_id": "142019050800009001",
    "merchant_order_id": "test100080"
}
```
<b>返回样例:</b>
```
{
	"code": 200000,
	"data": {
	   	"merchant_id": "142019050800009001",
	    "merchant_order_id": "test100080",
	    "order_id": "122020120813406020",
	    "order_status": "PC",
	    "order_amount": "50.80",
	    "order_currency": "THB",
	    "create_time": "2020-12-08 13:38:39"
	},
	"message": "Success",
	"trace_id": "54e2983e66b738e6"
}
```

#### 2.1.3. 支付结果通知 
>当支付确认完成时，如果商户支付参数中notify_url 不为空，连连泰国会推送支付结果至商户支付申请时填入的“notify_url”, 商户返回时，HTTP code必须为200。

触发频率: 10mins

最大通知次数：13

<b>通知参数列表（连连泰国 至 商户）:</b>

|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- | --- | --- | 
| order_id	| string(20)		|<center>Y</center>| 支付单ID| 
| merchant_order_id	| string(64)|<center>Y</center>| 商户订单ID|
| order_status	| string(8)		|<center>Y</center>| 	订单状态| 
| order_amount	| string (8,2)	|<center>Y</center>| 	订单金额| 
| order_currency| 	string (3)	|<center>Y</center>| 	币种| 
| create_time| 	string (19)		|<center>Y</center>| 	支付完成时间 | 

<b>Response Parameters (Merchant to Lianlian Thailand):</b>

|<div style="width:123px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- | --- | --- | 
|code|string(6)|<center>Y</center> | 固定: 200000|
|message|string(32)|<center>N</center> | 固定: Success|

<b>请求样例:</b>
```
{
	"order_id": "122020040700160021",
    "merchant_order_id": "test_020",
    "order_status": "PS",
    "order_amount": "100.00",
    "order_currency": "THB",
    "complete_time": "2020-04-07 13:19:02"

}
```
<b>返回样例：</b>
```
{
	"code": 200000,
	"message": "Success"
}
```

#### 2.1.4. 支付查询 

<b>请求参数列表:</b>

|<div style="width:122px" align="left"> 沙箱URL </div>|<div style="width:534px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>| 
| --- | --- | 
| <b>生产URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式</b> | GET|

|<div style="width:110px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- | --- | --- | 
| version | string(2) 		|<center>Y</center>| 接口版本号，固定值：v1| 
| service| string(64) 		|<center>Y</center>| 接口名称，固定值：llpth.wechatpay.pay.query| 
| merchant_id | string(20) 	|<center>Y</center>| 连连泰国提供的商户ID| 
| merchant_order_id |string(64）|<center>Y</center>|商户订单ID| 

<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess) 

|<div style="width:200px"> Response Parameters: Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:260px">Description</div>|
| --- | --- | --- | --- | 
| merchant_id| string(20)		|<center>Y</center>| 商户ID |
| merchant_order_id	| string(64)|<center>Y</center>| 支付交易ID|
| order_id	| string(20)		|<center>Y</center>| 订单ID| 
| order_status	| string(8)		|<center>Y</center>| 	订单状态| 
| order_amount	| string (8,2)	|<center>Y</center>| 	订单金额| 
| order_currency| 	string (3)	|<center>Y</center>| 	币种| 
| create_time| 	string (19)		|<center>Y</center>| 	创单时间 | 
| complete_time| 	string (19)	|<center>Y</center>| 支付完成时间 | 

<b>请求样例：</b>
```
https://sandbox-th.lianlianpay-inc.com/gateway?version=v1&service=llpth.wechatpay.pay.query&merchant_id=142019050800009001&merchant_order_id=test100085
```
<b>返回样例：</b>
```
{
	"code": 200000,
	"data": {
        "merchant_id": "142019050800009001",
        "merchant_order_id": "test100085",
        "order_id": "122020120913407003",
        "order_status": "PS",
        "order_amount": "0.15",
        "order_currency": "THB",
        "create_time": "2020-12-09 10:11:16",
        "complete_time": "2020-12-09 10:11:53"
	},
	"message": "Success",
	"trace_id": "c1c2e4fdb9abda7d"
}
```

### 2.2. 退款

#### 2.2.1. 退款请求 
<b>请求参数列表:</b>

|<div style="width:132px" align="left"> 沙箱URL </div>|<div style="width:535px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>|
| --- | --- | 
| <b>生产URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式</b> | POST|

|<div style="width:132px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- | --- | --- | 
| version | string(2) 		|<center>Y</center>| 接口版本号，固定值：v1 | 
| service| string(64) 		|<center>Y</center> |接口名称，固定值：llpth.wechatpay.refund| 
| merchant_id | string(20) 	|<center>Y</center>| 商户ID  | 
| merchant_refund_id| String(64)|<center>Y</center>| 商户退款单号，需要保证唯一性| 
|merchant_order_id |string(64）	|<center>Y</center>|商户原支付单ID| 
|refund_amount |	string(8, 2)|<center>Y</center>| 退款金额，不能大于支付金额 | 
|refund_currency	|string(3)	|<center>Y</center>| 退款币种 | 
| notify_url | string(256) 		|<center>N</center>| 退款结果通知地址 | 
| memo | string(80) 			|<center>N</center>| 备注信息  | 

<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess) 

|<div style="width:132px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- | --- | --- | 
| merchant_id | string(20) 			|<center>Y</center>| 商户ID  | 
| refund_serial _id| 	string(20)	|<center>Y</center>| 退款流水ID| 
| payment_order_id	| string(64)	|<center>Y</center>| 原支付单ID| 
| merchant_order_id| 	string(32)	|<center>Y</center>| 原商户支付单号| 
| merchant_refund_id| 	string(32)	|<center>Y</center>| 商户退款流水号| 
| refund_amount	| string(8,2)		|<center>Y</center>| 退款金额| 
| refund_currency	| string(3)		|<center>Y</center>|  币种 | 
| refund_status	| string(8)			|<center>Y</center>| R退款状态| 
| create_time| 	string (19)			|<center>Y</center>| 	退款时间| 
| complete_time	| string(19)		|<center>N</center>| 	退款时间| 


<b>请求样例：</b>
```
{
	"version": "v1",
	"service": "llpth.wechatpay.refund",
    "merchant_id": "142019050800009001",
    "merchant_order_id": "test100085",
    "merchant_refund_id": "refund100076",
    "refund_amount": "0.05",
    "refund_currency": "THB",
    "notify_url": "https://www.yezhou.cc/callback/notify.php",
    "memo": "merchant refund memo"
}
```

<b>返回样例：</b>
```
{
	"code": 200000,
	"data": {
        "refund_serial_id": "122020120913407011",
        "payment_order_id": "122020120913407003",
        "merchant_id": "142019050800009001",
        "merchant_refund_id": "refund100076",
        "merchant_order_id": "test100085",
        "refund_status": "RP",
        "refund_amount": "0.05",
        "refund_currency": "THB",
        "create_time": "2020-12-09 10:18:11"
	},
	"message": "Success",
	"trace_id": "4df57b46fe2af2c3"
}
```

#### 2.2.2. 退款结果通知 
>当退款完成时，连连泰国会推送退款结果至商户退款申请时填入的“notify_url”地址。商户返回时，HTTP code必须为200。

触发频率: 10mins

最大通知次数：13

<b>通知参数列表（连连泰国 至 商户）:</b>

|<div style="width:132px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- | --- | --- | 
| refund_order_id	| string(20)	|<center>Y</center>| 连连退款单ID| 
| merchant_refund_id	| string(64)|<center>Y</center>| Merchant refund order ID|
| refund_amount| 	string(8,2)		|<center>Y</center>| 退款金额| 
| refund_currency| 	string (3)		|<center>Y</center>| 币种 | 
| refund_status	| string (8)		|<center>Y</center>| 退款状态 | 
| complete_time	| string(19)		|<center>N</center>| 退款完成时间| 


<b>响应参数列表（商户 至 连连）：</b>

|<div style="width:132px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- | --- | --- | 
|code|string(6)		|<center>Y</center> | 固定: 200000|
|message|string(32)	|<center>N</center>|固定: Success|

<b>请求样例：</b>
```
{
	"refund_order_id": "122020040700160037",
    "merchant_refund_id": "test_refund_022",
    "refund_amount": "100.00",
    "refund_currency": "THB",
    "refund_status": "RS",
    "complete_time": "2020-04-07 14:09:38"
}
```
<b>返回样例：</b>
```
{
	"code": 200000,
	"message": "Success"
}
```

#### 2.2.3. 退款查询 

<b>请求参数列表:</b>

|<div style="width:132px" align="left"> 沙箱URL </div>|<div style="width:534px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>|
| --- | --- | 
| <b>生产URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式 </b> | GET|

|<div style="width:132px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:340px">Description</div>|
| --- | --- | --- | --- | 
| version | string(2) 		| <center>Y</center>| 接口版本号，固定值：v1 | 
| service| string(64) 		|<center>Y</center>| 接口名称，固定值：llpth.wechatpay.refund.query| 
| merchant_id | string(20) 	|<center>Y</center> | 连连泰国提供的商户ID | 
| merchant_refund_id |string(64）|<center>Y</center>|商户退款单号 | 

<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess) 

| Response Parameters: Field  | Type| Required  | <div style="width:340px">Description</div>|
| --- | --- | --- | --- | 
| merchant_id| string(20)		|<center>Y</center>| 商户ID|
| merchant_refund_id| string(64)|<center>Y</center>| 商户退款单ID| 
| merchant_order_id	| string(64)|<center>Y</center>| 商户原支付单ID| 
| refund_status	| string(20)	|<center>Y</center>| 退款状态| 
| refund_amount	| string(8, 2)	|<center>Y</center>| 退款金额，不能大于支付金额| 
| refund_currency	| string(3)	|<center>Y</center>| 退款币种 | 
| create_time| 	string (19)		|<center>Y</center>| 	退款发起时间 | 
| complete_time| 	string (19)	|<center>N</center>| 退款完成时间 | 
| memo | string(1024) 			|<center>N</center>| 退款原因| 


<b>请求样例：</b>
```
https://sandbox-th.lianlianpay-inc.com/gateway?version=v1&service=llpth.wechatpay.refund.query&merchant_id=142019050800009001&merchant_refund_id=refund100076
```
<b>返回样例：</b>
```
{
	"code": 200000,
    "data": {
        "merchant_id": "142019050800009001",
        "merchant_order_id": "test100085",
        "merchant_refund_id": "refund100076",
        "refund_currency": "THB",
        "refund_amount": "0.05",
        "refund_status": "RS",
        "create_time": "2020-12-09 10:18:11",
        "complete_time": "2020-12-09 10:18:21",
        "memo": "test"
	},
	"message": "Success",
    "trace_id": "78a74d643a9a0106"
}
```

## 3.  附录 <!-- {docsify-ignore} -->
> [详见](./#Appendix)