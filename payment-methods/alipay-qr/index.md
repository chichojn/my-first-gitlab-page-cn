# 连连泰国支付网关（支付宝扫码）接入手册



## 1. 集成方式 
>连连泰国提供 API 集成方式。

### 1.1. 交互流程   
<div align="center">
<img width="600" src="./images/alipay/qr-flow.PNG"/>
</div>

## 2. 接口定义  
### 2.1. 支付 
>商户订单ID需要保证唯一，不允许发起重复提交。
### 2.1.1. 支付请求 

<b>Request Parameters</b>

|<div style="width:130px" align="left"> 沙箱 </div>|<div style="width:530px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>|  
| --- | --- | 
| <b>生产URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式 </b> | POST|

|<div style="width:130px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:335px">Description</div>|
| --- | --- | --- | --- | 
|version	|string(2)			|<center>Y</center>|接口版本号，固定值：v1 |
|service	|string(64)			|<center>Y</center>|接口名称，固定值：llpth.alipay.offline.pay|
|merchant_id	|String(20)		|<center>Y</center>|商户ID  |
|merchant_order_id	|string(64）|<center>Y</center>|商户交易ID  |
|order_amount	|string(8, 2)	|<center>Y</center>|订单金额 |
|order_currency	|string(3)		|<center>Y</center>|币种|
|order_info	|string(512)		|<center>Y</center>|向买家展示的信息|
|store_id	|string(18)			|<center>Y</center>|门店ID,门店的唯一标识|
|payment_type	|string(32)		|<center>Y</center>|支付方式MERCHANT_SCAN, DYNAMIC_CODE|
|buyer_identity_code	|string(32)	|<center>Y</center>|用户支付条码|
|notify_url	|string(256)		|<center>N</center>|支付结果通知地址|


<b>响应参数列表:</b>

 >通用响应字段 [详见](./#GenResMess) 

|<div style="width:130px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:335px">Description</div>|
| --- | --- | --- | --- | 
|order_id	|string(20)			|<center>Y</center>|支付单ID|
|order_status	|string(8)		|<center>Y</center>|订单状态|
|order_amount	|string (8,2)	|<center>Y</center>|订单金额|
|order_currency	|string (3)		|<center>Y</center>|币种|
|create_time	|string (19)	|<center>Y</center>|订单时间|
|link_url	|text				|<center>N</center>|订单码的二维码,商户主扫为空|


<b>请求样例</b>
```
{
	"version": "v1",
	"service": "llpth.alipay.offline.pay",
	"merchant_id": "142019050800009001",
	"merchant_order_id": "test_020",
	"order_amount": "100.12",
	"order_currency": "THB",
	"order_info": "display your order info",
	"payment_type ": "MERCHANT_SCAN",
	"store_id": "142020050900009001",
	"buyer_identity_code": "280481234296932218",
	"notify_url": "https://www.lianlianpay.co.th/callback"
}
```

<b>返回样例</b>
```
{
	""code": 200000,
	"data": {
	"order_id": "122020040700160021",
	"order_status": "WP",
	"order_amount": "100.12",
	"order_currency": "THB",
	"create_time": "2020-04-07 13:19:02",
	"link_url": ""
    },
    "message": "Success",
    "trace_id": "54e2983e66b738e6"
}
```

#### 2.1.2. 支付取消 

<b>请求参数列表:</b>

|<div style="width:130px" align="left"> 沙箱 </div>|<div style="width:530px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>| 
| --- | --- | 
| <b>生产URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式</b> | POST|

|<div style="width:130px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:335px">Description</div>|
| --- | --- | --- | --- | 
|version	|string(2)		|<center>Y</center>|接口版本号，固定值：v1|
|service	|string(64)	|<center>Y</center>|接口名称，固定值：|
|merchant_id	|String(20)	|<center>Y</center>|商户ID  |
|merchant_order_id	|string(64）	|<center>Y</center>|商户交易ID  |

<b>响应参数列表:</b>
 >通用响应字段 [详见](./#GenResMess) 


<b>请求样例</b>
```
{
	"version": "v1",
	"service": "llpth.alipay.offline.cancel",
	"merchant_id": "142019050800009001",
	"merchant_order_id": "test_020"
}
```
<b>返回样例</b>
```
{
	"code": 200000,
    "message": "Success",
    "trace_id": "54e2983e66b738e6"
}
```

#### 2.1.3. 支付结果通知 

>当支付确认完成时，如果商户支付参数中notify_url 不为空，连连泰国会推送支付结果至商户支付申请时填入的“notify_url”, 商户返回时，HTTP code必须为200。

触发频率: 10mins

最大通知次数：13


<b>通知参数列表（连连泰国 至 商户）:</b>

|<div style="width:130px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:335px">Description</div>|
| --- | --- | --- | --- | 
| order_id	| string(20)		|<center>Y</center>| 支付单ID| 
| merchant_order_id	| string(64)|<center>Y</center>| 商户订单ID|
| order_status	| string(8)		|<center>Y</center>| 订单状态| 
| order_amount	| string (8,2)	|<center>Y</center>| 订单金额| 
| order_currency| 	string (3)	|<center>Y</center>| 币种| 
| complete_time| 	string (19)	|<center>Y</center>| 支付完成时间| 

<b>响应参数列表（商户 至 连连泰国）：</b>

|<div style="width:130px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:335px">Description</div>|
| --- | --- | --- | --- | 
|code|string(6)		|<center>Y</center>| 固定: 200000|
|message|string(32)	|<center>N</center> | 固定: Success|

<b>请求样例</b>
```
{
	"order_id": "122020040700160021",
	"merchant_order_id": "test_020",
	"order_status": "PS",
     "order_amount": "100.00",
	"order_currency": "THB",
    "complete_time": "2020-04-07 13:19:02"     

}
```
<b>返回样例</b>
```
{
	"code": 200000,
    "message": "Success"
}
```

#### 2.1.4. 支付查询 

<b>请求参数列表:</b>

|<div style="width:130px" align="left"> 沙箱URL </div>|<div style="width:530px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>| 
| --- | --- | 
| <b>生产URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式</b> | GET|

|<div style="width:130px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:335px">Description</div>|
| --- | --- | --- | --- | 
| version | string(2) 		|<center>Y</center>| 接口版本号，固定值：v1 | 
| service| string(64) 		|<center>Y</center>| 接口名称，固定值：llpth.alipay.offline.pay.query| 
| merchant_id | string(20) 	|<center>Y</center>| 连连泰国提供的商户ID| 
| merchant_order_id |string(64）|<center>Y</center>|商户订单ID| 

<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess) 

|<div style="width:200px">Response Parameters: Field </div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:265px">Description</div>|
| --- | --- | --- | --- | 
| merchant_id| string(20)		|<center>Y</center>| 商户ID |
| merchant_order_id	| string(64)|<center>Y</center>| 支付交易ID|
| order_id	| string(20)		|<center>Y</center>| 订单ID| 
| order_status	| string(8)		|<center>Y</center>| 订单状态| 
| order_amount	| string (8,2)	|<center>Y</center>| 订单金额| 
| order_currency| 	string (3)	|<center>Y</center>| 币种| 
|create_time	|string(19)		|<center>Y</center>|创单时间 |
|complete_time	|string(19)		|<center>N</center>|支付完成时间|



<b>请求样例</b>
```
https://sandbox-th.lianlianpay-inc.com/gateway?version=v1&service=llpth.alipay.offline.pay.query&merchant_id=142019050800009001&merchant_order_id=test_019
```
<b>返回样例</b>
```
{
    "code": 200000,
	"data": {
		"merchant_id": "142019050800009001",
	    	"merchant_order_id": "test_019",
	"order_id": "122020040700160016",
	"order_status": "PS",
	"order_amount": "100.00",
	"order_currency": "THB",
	"create_time": "2020-04-07 10:38:18",
	"complete_time": "2020-04-07 10:41:26"
    },
    "message": "Success",
    "trace_id": "c1c2e4fdb9abda7d"
}
```

### 2.2. 退款

#### 2.2.1. 退款请求 
<b>请求参数列表:</b>

|<div style="width:140px" align="left"> 沙箱URL </div>|<div style="width:520px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>| 
| --- | --- | 
| <b>生产URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式 </b> | POST|

|<div style="width:140px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:325px">Description</div>|
| --- | --- | --- | --- | 
|version	|string(2)			|<center>Y</center>|接口版本号，固定值：v1|
|service	|String(64)			|<center>Y</center>|接口名称，固定值：llpth.alipay.offline.refund|
|merchant_id	|string(20)		|<center>Y</center>|商户ID |
|merchant_refund_id	|String(64)	|<center>Y</center>|商户退款单号，需要保证唯一性|
|merchant_order_id	|string(64)	|<center>Y</center>|商户原支付单ID|
|refund_amount	|string(8, 2)	|<center>Y</center>|退款金额，不能大于支付金额|
|refund_currency	|string(3)	|<center>Y</center>|退款币种 |
|notify_url	|string(256)		|<center>N</center>|退款结果通知地址|


<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess) 

|<div style="width:140px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:325px">Description</div>|
| --- | --- | --- | --- | 
|merchant_id	|string(20)		|<center>Y</center>|商户ID |
|refund_serial _id	|string(20)	|<center>Y</center>|退款流水ID|
|payment_order_id	|string(64)	|<center>Y</center>|原支付单ID|
|merchant_order_id	|string(32)	|<center>Y</center>|原商户支付单号|
|merchant_refund_id	|string(32)	|<center>Y</center>|商户退款流水号|
|refund_amount	|string(8,2)	|<center>Y</center>|退款金额|
|refund_currency	|string(3)	|<center>Y</center>|币种 |
|refund_status	|string(8)		|<center>Y</center>|退款状态|
|memo	|string(256)			|<center>N</center>|备注|
|create_time	|string(19)		|<center>Y</center>|退款时间|


<b>请求样例</b>
```
{
	"version": "v1",
	"service": " llpth.bankcard.refund",
	"merchant_id": "142019050800009001",
	"merchant_refund_id": "merchant_refund_022",
	"merchant_order_id": "merchant_payment_003",
	"refund_amount": "100.00",
	"refund_currency": "THB",
	"notify_url": "http://www.lianlanpay.co.th/callback",
	"memo": "merchant refund demo"
}
```

<b>返回样例</b>
```
{
	"code": 200000,
    "message": "Success",
    "data": {
		"merchant_id": "142019050800009001",
		"refund_serial_id": "122019060500018005",
		"payment_order_id": "122019060500019005",
		"merchant_refund_id": "merchant_refund_022",
		"merchant_order_id": "merchant_payment_003",
		"refund_amount": "100.00",
		"refund_currency": "THB",
		"refund_status": "RP",
		"memo": "merchant refund demo",
		"creation_time": "2020-08-12 23:34:12"
    	},
	"trace_id": "4df57b46fe2af2c3"
}
```

#### 2.2.2. 退款结果通知 
>当退款完成时，连连泰国会推送退款结果至商户退款申请时填入的“notify_url”地址。商户返回时，HTTP code必须为200。

触发频率: 10mins

最大通知次数：13

<b>Notification Parameters (Lianlian Thailand to merchant):</b>

|<div style="width:140px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:325px">Description</div>|
| --- | --- | --- | --- | 
| refund_order_id	| string(20)	|<center>Y</center>| 连连退款单ID| 
| merchant_refund_id	| string(64)|<center>Y</center>| 商户退款单ID|
| refund_amount| 	string(8,2)		|<center>Y</center>| 退款金额| 
| refund_currency| 	string (3)		|<center>Y</center>| 币种 | 
| refund_status	| string (8)		|<center>Y</center>| 退款状态 | 
| complete_time	| string(19)		|<center>N</center>	|退款完成时间| 


<b>响应参数列表（商户 至 连连）：</b>

|<div style="width:140px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:325px">Description</div>|
| --- | --- | --- | --- | 
|code|string(6)		|<center>Y</center>| 固定: 200000|
|message|string(32)	|<center>N</center>| 固定: Success|

<b>请求样例</b>
```
{
	"refund_order_id": "122020040700160037",
    "merchant_refund_id": "test_refund_022",
    "refund_amount": "100.00",
    "refund_currency": "THB",
    "refund_status": "RS",
    "complete_time": "2020-04-07 14:09:38"
}
```
<b>R返回样例</b>
```
{
	"code": 200000,
	"message": "Success"
}
```

#### 2.2.3. 退款查询 

<b>请求参数列表:</b>

|<div style="width:140px" align="left"> 沙箱URL </div>|<div style="width:526px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>| 
| --- | --- | 
| <b>生产URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式</b> | GET|

|<div style="width:140px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:332px">Description</div>|
| --- | --- | --- | --- | 
| version | string(2) 		|<center>Y</center>| 接口版本号，固定值：v1 | 
| service| string 			|<center>Y</center>| 接口名称，固定值：<br>llpth.alipay.offline.refund.query| 
| merchant_id | string(20) 	|<center>Y</center>| 连连泰国提供的商户ID | 
| merchant_refund_id |string(64）|<center>Y</center>|商户退款单号 | 

<b>Field Response Parameters:</b>

> 通用响应字段 [详见](./#GenResMess) 

| 响应参数列表  | Type| Required  | Description|
| --- | --- | --- | --- |  
| merchant_id| string(32)		|<center>Y</center>|商户ID |
| merchant_refund_id| string(64)|<center>Y</center>| 商户退款单ID| 
| merchant_order_id	| string(64)|<center>Y</center>| 商户原支付单ID| 
|refund_status	|string(8)		|<center>M</center>|退款状态|
|refund_amount	|string(8, 2)	|<center>Y</center>|退款金额，不能大于支付金额|
|refund_currency	|string(3)	|<center>Y</center>|退款币种 |
|create_time	|string(19)		|<center>Y</center>|退款发起时间|
|complete_time	|string(19)		|<center>N</center>|R退款完成时间|
|memo	|string(1024)			|<center>N</center>||



<b>请求样例:</b>
```
https://sandbox-th.lianlianpay-inc.com/gateway?version=v1&service=llpth.alipay.offline.refund.query&merchant_id=142019050800009001&merchant_refund_id=test_refund_022
```
<b>返回样例：</b>
```
{
    "code": 200000,
	"data": {
		"merchant_id": "142019050800009001",
		"merchant_refund_id": "test_refund_022",
		"merchant_order_id": "test_019",
		"refund_status": "RS",
		"refund_amount": "100.00",
		"refund_currency": "THB",
	    "create_time": "2020-04-07 14:09:38",        
	    "complete_time": "2020-04-07 14:09:40",
		"memo": "refund memo"
    },
    "message": "Success",
    "trace_id": "78a74d643a9a0106"
}
```

## 3. 附录 <!-- {docsify-ignore} -->
> [详见](./#Appendix)