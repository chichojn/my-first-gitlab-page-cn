# 连连泰国支付网关（银行卡）接入手册


## 1. 集成方式 
>连连泰国提供Direct API和Checkout Page两种集成方式。有PCI资质的商户两种对接方式均可选择，无PCI资质的商户请使用Checkout Page方式进行对接。

#### 1.1. Direct API 
<div align="center">
<img width="700" src="./images/bankcard/direct-api.PNG"/>
</div>

#### 1.2. Checkout Page
<div align="center">
<img width="700" src="./images/bankcard/checkout-page.PNG"/>
</div>

!> 注：跳转连连收银台时会通过HTTP referer头校验商户域名或IP信息

## 2. 对象定义 
<a name="Product" id="product"></a>
### 2.1. Product
|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:360px">Description</div>|
| --- | --- | --- | --- | 
|name	|string(128)		|<center>Y</center>	|商品名称| 
|description	|string(128)|<center>N</center>|商品描述，不允许包含”’等特殊字符|
|unit_price	|string(15,2)	|<center>Y</center>|商品单价，需保留两位小数 Eg.100.00|
|quantity	|string(10)		|<center>Y</center>	|商品数量，正整数|
|category	|string (64)	|<center>N</center>	|商品分类|
|show_url	|string(256)	|<center>N</center>	|商品网址，支付方式为国际信用卡时必须|

<a name="Address" id="address"></a>
### 2.2. Address 
|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:360px">Description</div>|
| --- | --- |--- |--- |
|street_name	|string(128)	|<center>N</center>	|客户街道名称|
|house_number	|string(32)	|<center>N</center>	|客户街道门牌号|
|district	|string(32)	|<center>N</center>	|区|
|city	|string(64)	|<center>N</center>	|城市|
|state	|string(2)	|<center>N</center>	|省份，缩写。|
|country	|string(2)	|<center>N</center>	|省份，缩写。|
|postal_code	|string(16)	|<center>N</center>	|邮编|

<a name="Customer" id="customer"></a>
### 2.3. Customer 
|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:355px">Description</div>|
| --- | --- |--- |--- |
|merchant_user_id|	string(64)	|<center>Y</center>	|买家在商户中的唯一标志|
|customer_type	|string(1)	|<center>N</center>	|客户身份类型，目前仅支持个人<br>I =Individual（个人）<br>C=Corporation（公司）|
|first_name	|string(64)|	<center>N</center>	|Only for individual|
|last_name	|string(64)	|<center>N</center>	|Only for individual|
|full_name	|string(128)	|<center>Y</center>	|个人全名或者公司名称|
|gender	|string(16)	|<center>N</center>	|性别: MALE, FEMALE, UNKNOWN|
|id_type	|string(16)|	<center>N</center>	|证件类型|
|id_no	|string(32)	|<center>N</center>	|证件号|
|email	|string(64)|	<center>N</center>	|客户邮件地址|
|phone	|string(32)|	<center>N</center>	|格式：”+国家或者区号-手机号”|
|company	|string(128)|	<center>N</center>	|个人公司名称|
|address	|Address| <center>N</center>	|地址信息，, [详见 2.2](#Address)|

<a name="Card" id="card"></a>
### 2.4. Card 
|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:360px">Description</div>|
| --- | --- |--- |--- |
|holder_name	|string(128)	|<center>Y</center>	|持卡人姓名|
|card_no	|string(32)	|<center>Y</center>	|卡号|
|card_type	|string(2)	|<center>Y</center>	|卡类型：D=借记，C=贷记|
|bank_code	|string(32)	|<center>N</center>	|银行编码|
|brand	|string(32)	|<center>N</center>	|信用卡卡品牌 : visa/mastercard/jcb/Union pay|
|exp_year	|string(2)	|<center>Y</center>	|卡年有效期|
|exp_month	|string(2)	|<center>Y</center>	|卡月有效期|
|cvv2	|string(3)	|<center>N</center>	|卡安全码|
|billing_address	|Address |<center>N</center>	|[详见 2.2](#Address)|


## 3. 接口定义 API
### 3.1. 支付 
>商户订单ID需要保证唯一，非PI/WP状态订单不允许发起重复提交。

对于重复提交的商户支付订单请求，出于信息安全考虑需要保证以下字段与初次提交相同。

非银行卡信息依然沿用第一次提交的信息。

#### 3.1.1. Direct API支付请求 

<b>请求参数列表:</b>

|<div style="width:122px" align="left"> 沙箱 URL </div>|<div style="width:542px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>| 
| --- | --- | 
| <b>生产 URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式 </b> | POST|

|<div style="width:110px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:335px">Description</div>|
| --- | --- | --- | --- | 
|version 	|String	|<center>Y</center>	|接口版本号，固定值：v1|
|service	|String	|<center>Y</center>	|接口名称，固定值: llpth.bankcard.pay|
|merchant_id	|String(20)	|<center>Y</center>	|商户ID |
|merchant_order_id	|string(64）	|<center>Y</center>	|商户交易 ID |
|order_amount	|string(8, 2)	|<center>Y</center>	|订单金额  |
|order_currency	|string(3)	|<center>Y</center>	|币种|
|order_desc	|string(256)	|<center>Y</center>	|向买家展示的信息|
|payment_method	|string(32)	|<center>Y</center>	|支付方式:<br>CARD(信用卡/借记卡)<br>CREDIT_CARD(信用卡)<br>
DEBIT_CARD(借记卡)|
|customer |Customer |<center>Y</center>	|[客户信息详见](#Customer)|
|products	|List Product |<center>N</center>	|[商品信息](#Product)|
|card	|Card |<center>N</center>	|[银行卡信息](#Card) <br>银行 卡支付必传 |
|notify_url	|string(256)	|<center>Y</center>	|支付结果通知地址|
|redirect_url	|string(256)	|<center>Y</center>	|支付成功后，用户页面回跳URL地址|

<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess)

|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:348px">Description</div>|
| --- | --- | --- | --- | 
|merchant_id	|string(20)	|<center>Y</center>	|商户ID |
|merchant_order_id	|string(64）|	<center>Y</center>	|商户订单ID|
|order_id	|string(20)	|<center>Y</center>	|支付单ID|
|order_status	|string(8)	|<center>Y</center>	|订单状态|
|order_amount	|string (8,2)	|<center>Y</center>	|订单金额|
|order_currency	|string (3)	|<center>Y</center>	|币种|
|create_time	|string (19)	|<center>Y</center>	|订单时间|
|link_url	|string(256)	|<center>Y</center>|发卡行地址|

<b>请求样例</b>
```
{
	"version": "v1",
	"service": "llpth.bankcard.pay",
	"merchant_id": "142019050800009001",
	"merchant_order_id": "test_020",
	"order_amount": "100.00",
	"order_currency": "THB",
	"order_desc": "display your order info",
	"payment_method": "CARD",
	"customer": {
		"merchant_user_id": "m_user_01", 
		"full_name": "coba"
	},
	"card": {
		"holder_name": "user_name", 
		"card_no": "5404888000072727", 
		"card_type": "C", 
		"exp_year": "23", 
		"exp_month": "12",
		"cvv2": "123"
	},
	"notify_url": "https://merchant_notify_url",
	"redirect_url": "https://merchant_redirect_url"
}
```

<b>返回样例</b>
```
{
	"code": 200000,
	"data": {
		"merchant_id": "142019050800009001",
		"merchant_order_id": "test_020",
		"order_id": "122020040700160021",
		"order_status": "PI",
		"order_amount": "100.00",
		"order_currency": "THB",
		"create_time": "2020-04-07 13:19:02",
		"link_url": "bank_url"
    },
    "message": "Success",
    "trace_id": "54e2983e66b738e6"
}
```

#### 3.1.2. Checkout Page 支付请求 
<b>请求参数列表:</b>

|<div style="width:122px" align="left"> 沙箱 URL </div>|<div style="width:542px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>| 
| --- | --- | 
| <b>生产 URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式</b> | POST|

|<div style="width:110px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:348px">Description</div>|
| --- | --- | --- | --- | 
|version	|String	|<center>Y</center>	|接口版本号，固定值：v1|
|service	|String	|<center>Y</center>	|接口名称，固定值：llpth.checkout.apply, <br>推荐接入了连连泰国多个支付产品的商户使用|
|merchant_id	|String(20)	|<center>Y</center>	|商户ID|
|merchant_order_id	|string (64）	|<center>Y</center>	|商户交易ID |
|order_amount	|string (8, 2)	|<center>Y</center>	|订单金额|
|order_currency	|string (3)	|<center>Y</center>	 |币种 |
|order_desc	|string (256)	|<center>Y</center>	|C收银台展示信息 |
|customer |Customer |<center>Y</center>	|[客户信息详见](#Customer)|
|products	|List Product |<center>N</center>	|[商品信息](#Product)|
|card	|Card |<center>N</center>	|[银行卡信息](#Card) <br>银行 卡支付必传 |
|notify_url	|string(256)	|<center>Y</center>	|支付结果通知地址|
|redirect_url	|string(256)	|<center>Y</center>	|支付成功后，用户页面回跳URL地址|

<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess)

|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:348px">Description</div>|
| --- | --- | --- | --- | 
|merchant_id	|string(20)	|<center>Y</center>	|商户ID |
|merchant_order_id	|string(64）|	<center>Y</center>	|商户订单ID |
|order_id	|string(20)	|<center>Y</center>	|支付单ID|
|order_status	|string(8)	|<center>Y</center>	|订单状态|
|order_amount	|string (8,2)	|<center>Y</center>	|订单金额|
|order_currency	|string (3)	|<center>Y</center>	|币种 |
|create_time	|string (19)	|<center>Y</center>	|订单时间|
|link_url	|string(256)	|<center>Y</center>|收银台地址|

<b>请求样例</b>

```
{
	"version": "v1",
	"service": "llpth.checkout.apply",
	"merchant_id": "142019050800009001",
	"merchant_order_id": "test_020",
	"order_amount": "100.00",
	"order_currency": "THB",
	"order_desc": "display your order info",
	"customer": {
		"merchant_user_id": "m_user_01",
		"full_name": "coba"
	},
	"notify_url": "https://merchant_notify_url",
	"redirect_url": "https://merchant_redirect_url"
}
```

<b>返回样例</b>
```
{
	"code": 200000,
	"data": {
		"merchant_id": "142019050800009001",
		"merchant_order_id": "test_020",
		"order_id": "122020040700160021",
		"order_status": "PI",
		"order_amount": "100.00",
		"order_currency": "THB",
		"create_time": "2020-04-07 13:19:02",
		"link_url": " https://sandbox-checkout.lianlianpay-inc.com?nonce=prjbcmxdght4e0i1l&pms=Y2FyZF8x"
    },
    "message": "Success",
    "trace_id": "54e2983e66b738e6"
}
```

#### 3.1.3. Bankcard Checkout Page 支付请求
<b>请求参数列表:</b>

|<div style="width:122px" align="left"> 沙箱 URL </div>|<div style="width:542px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>| 
| --- | --- | 
| <b>生产 URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式</b> | POST|

|<div style="width:110px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:348px">Description</div>|
| --- | --- | --- | --- | 
|version	|String	|<center>Y</center>	|接口版本号，固定值: v1|
|service	|String	|<center>Y</center>	|接口名称，固定值： llpth.checkout.apply,<br> 推荐只接入了连连泰国银行卡支付产品的商户使用|
|merchant_id	|String(20)	|<center>Y</center>	|商户ID|
|merchant_order_id	|string (64）	|<center>Y</center>	|商户交易ID |
|order_amount	|string (8, 2)	|<center>Y</center>	|订单金额|
|order_currency	|string (3)	|<center>Y</center>	 |币种 |
|order_desc	|string (256)	|<center>Y</center>	|收银台展示信息 |
|payment_method	|string(32)	|<center>Y</center>	|支付方式 :<br>CARD(信用卡/借记卡)<br>CREDIT_CARD(信用卡)<br>DEBIT_CARD(借记卡)|
|customer |Customer |<center>Y</center>	|[客户信息详见](#Customer)|
|products	|List Product |<center>N</center>	|[商品信息](#Product)|
|card	|Card |<center>N</center>	|[银行卡信息](#Card) <br>银行 卡支付必传 |
|notify_url	|string(256)	|<center>Y</center>	|支付结果通知地址|
|redirect_url	|string(256)	|<center>Y</center>	|支付成功后，用户页面回跳URL地址|

<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess)

|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:348px">Description</div>|
| --- | --- | --- | --- | 
|merchant_id	|string(20)	|<center>Y</center>	|商户ID|
|merchant_order_id	|string(64）|	<center>Y</center>	|商户订单ID|
|order_id	|string(20)	|<center>Y</center>	|支付单ID|
|order_status	|string(8)	|<center>Y</center>	|订单状态|
|order_amount	|string (8,2)	|<center>Y</center>	|订单金额|
|order_currency	|string (3)	|<center>Y</center>	|币种|
|create_time	|string (19)	|<center>Y</center>	|订单时间|
|link_url	|string(256)	|<center>Y</center>|收银台地址|

<b>请求样例：</b>
```
{
	"version": "v1",
	"service": "llpth.bankcard.checkout.apply",
	"merchant_id": "142019050800009001",
	"merchant_order_id": "test_020",
	"order_amount": "100.00",
	"order_currency": "THB",
	"order_desc": "display your order info",
	"payment_method": "CARD",
	"customer": {
		"merchant_user_id": "m_user_01",
		"full_name": "coba"
	},
	"notify_url": "https://merchant_notify_url",
	"redirect_url": "https://merchant_redirect_url"
}
```

<b>返回样例:</b>
```
{
	"code": 200000,
	"data": {
		"merchant_id": "142019050800009001",
		"merchant_order_id": "test_020",
		"order_id": "122020040700160021",
		"order_status": "PI",
		"order_amount": "100.00",
		"order_currency": "THB",
		"create_time": "2020-04-07 13:19:02",
		"link_url": " https://sandbox-checkout.lianlianpay-inc.com?nonce=prjbcmxdght4e0i1l&pms=Y2FyZF8x"
	},
    "message": "Success",
    "trace_id": "54e2983e66b738e6"
}
```

#### 3.1.4. 支付结果通知 
>当支付确认完成时，连连泰国会推送支付结果至商户支付申请时填入的“notify_url”, 商户返回时，HTTP code必须为200。

触发频率: 10mins

最大通知次数：13

<b>通知参数列表（连连泰国 至 商户）:</b>

|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:348px">Description</div>|
| --- | --- | --- | --- | 
|order_id	|string(20)	|<center>Y</center>	|支付单 ID|
|merchant_order_id	|string(64)	|<center>Y</center>	|商户订单 ID|
|order_status	|string (8)	|<center>Y</center>	|订单状态|
|order_amount	|string(8,2)	|<center>Y</center>	|订单金额|
|order_currency	|string (3)	|<center>Y</center>	|币种|
|complete_time	|string (19)	|<center>N</center>	|支付完成时间|

<b>响应参数列表（商户 至 连连泰国）:</b>

|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:360px">Description</div>|
| --- | --- | --- | --- | 
|code|string(6)|Y | 固定: 200000|
|message|string(32)|N | 固定: Success|

<b>请求样例：</b>
```
{
	"order_id": "122020040700160021",
	"merchant_order_id": "test_020",
	"order_status": "PS",
    "order_amount": "100.00",
	"order_currency": "THB",
    "complete_time": "2020-04-07 13:19:02"
}
```
<b>返回样例：</b>
```
{
	"code": 200000,
	"message": "Success"
}
```

#### 3.1.5. 支付查询 

<b>请求参数列表:</b>

|<div style="width:122px" align="left"> 沙箱 URL </div>|<div style="width:542px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>|  
| --- | --- | 
| <b>生产 URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式 </b> | GET|

|<div style="width:110px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:348px">Description</div>|
| --- | --- | --- | --- | 
| version | string | <center>Y</center> | 接口版本号，固定值：v1 | 
| service| string |<center>Y</center> | 接口名称，固定值：llpth.bankcard.pay.query| 
| merchant_id | string(20) | <center>Y</center> | 连连泰国提供的商户ID | 
| merchant_order_id |string(64）|	<center>Y</center>|商户订单 ID | 

<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess)

| Response Parameters: Field  | Type| Required  | <div style="width:300px">Description</div>|
| --- | --- | --- | --- | 
| merchant_id| string(20)| <center>Y</center>| 商户ID |
| merchant_order_id	| string(64)|<center>Y</center>| 支付交易ID|
| order_id	| string(20)| <center>Y</center>	| 订单 ID| 
| order_status	| string(8)| 	<center>Y</center>| 订单状态| 
| order_amount	| string (8,2)| <center>Y</center>| 订单金额| 
| order_currency| 	string (3)| <center>Y</center>| 币种| 
| order_desc	| string(256) | <center>Y</center>| 收银台展示信息 | 
|product_code	|string(64)	|<center>Y</center>	|产品编码 |
|payment_method	|string(64)	|<center>Y</center>  | 支付方式 |
|customer |Customer |<center>Y</center>	|[客户信息详见](#Customer)|
|products	|List Product |<center>N</center>	|[商品信息](#Product)|
|create_time	|string(19)	|<center>Y</center>|订单时间 |
|complete_time	|string(19)	|<center>N</center>	|支付完成时间|

<b>请求样例</b>
```
https://sandbox-th.lianlianpay-inc.com/gateway?version=v1&service=llpth.bankcard.pay.query&merchant_id=142019050800009001&merchant_order_id=test_019
```
<b>返回样例</b>
```
{
	"code": 200000,
	"data": {
		"merchant_id": "142019050800009001",
    	"merchant_order_id": "test_019",
		"order_id": "122020040700160016",
		"order_status": "PS",
		"order_amount": "100.00",
		"order_currency": "THB",
		"order_desc": "display your order info",
		"product_code": "BANKCARD_PAY_ONLINE",
		"payment_method": "CARD",
		"customer": {
		"merchant_user_id": "m_user_01",
		"full_name": "coba"
	},
		"products": [{
			"name": "mobile phone shell",
			"unit_price": "100.00",
			"quantity": "1",
		}],
		"create_time": "2020-04-07 10:38:18",
		"complete_time": "2020-04-07 10:41:26"
    },
    "message": "Success",
    "trace_id": "c1c2e4fdb9abda7d"
}
```

#### 3.1.6. 支付完成跳转商户页面
>支付完成时，连连泰国会根据商户支付申请时填入的“redirect_url”跳转回商户。跳转方式为HTTP POST form表单。除sign_type和sign外，对其它参数加签。订单最终状态商户应以异步通知或查询为准。                     

<b>参数列表（连连泰国 至 商户）:</b>

|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:348px">Description</div>|
| --- | --- | --- | --- | 
| merchant_order_id	| string(64)	| <center>Y</center>	| 商户支付订单 ID| 
| order_id	| string(20)	| <center>Y</center>| 	连连订单 ID| 
| order_status	| string(8)| 	<center>Y</center>	| 订单状态 | 
| order_amount	| string(8,2)	| <center>N</center>	|订单金额| 
| order_currency	| string(3)| 	<center>N</center>	|  币种 | 
| sign_type	| string(3)	| <center>Y</center>	| 固定 RSA| 
| sign	| String(256)	| <center>Y</center>	| 签名 | 

### 3.2. 退款
#### 3.2.1. 退款请求 

<b>请求参数列表:</b>

|<div style="width:129px" align="left"> 沙箱 URL </div>|<div style="width:534px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>|  
| --- | --- | 
| <b>生产 URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式 </b> | POST|

|<div style="width:110px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:338px">Description</div>|
| --- | --- | --- | --- | 
| version | string | <center>Y</center> | 接口版本号，固定值：v1 | 
| service| string |<center>Y</center> | 接口名称，固定值: llpth.bankcard.pay.query| 
| merchant_id | string(20) | <center>Y</center> | 商户ID | 
| merchant_refund_id | string(64) | <center>Y</center> | 商户退款单号，需要保证唯一性 | 
| merchant_order_id |string(64）|	<center>Y</center>|商户原支付单ID | 
| refund_amount	| string(8, 2)	| <center>Y</center>	| 退款金额，不能大于支付金额| 
| refund_currency	| string(3)	| <center>Y</center>	| 退款币种| 
| refund_reason| 	string(256)| 	<center>N</center>	| 退款原因| 
| notify_url	| string(256)| 	<center>N</center>	| 退款结果通知地址| 


<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess)

| Response Parameters: Field  | Type| Required  | <div style="width:308px">Description</div>|
| --- | --- | --- | --- | 
| merchant_id| string(20)| <center>Y</center>| 商户ID  |
| refund_order _id	|string(20)	|<center>Y</center>	|退款单ID|
| merchant_refund_id	|string(64)	|<center>Y</center>	|商户退款 ID|
| refund_amount	|string(8,2)	|<center>Y</center>	|退款金额|
| refund_currency	|string(3)	|<center>Y</center>	 |币种 |
| refund_status	|string(8)	|<center>Y</center>	|退款状态|
| create_time	|string(19)	|<center>Y</center>	|退款时间|


<b>请求样例:</b>
```
}
	"version": "v1",
	"service": "llpth.bankcard.refund",
	"merchant_id": "142019050800009001",
	"merchant_refund_id": "test_refund_022",
	"merchant_order_id": "test_019",	
	"refund_amount": "100.00",
	"refund_currency": "THB",
	"refund_reason": "refund reason",
	"notify_url": ""
}
```
<b>返回样例:</b>
```
{
	"code": 200000,
    "message": "Success",
    "data": {
		"merchant_id": "142019050800009001",
	    "refund_order_id": "122020040700160037",
	    "merchant_refund_id": "test_refund_022",
	    "refund_amount": "100.00",
	    "refund_currency": "THB",
	    "refund_status": "RP",
	    "create_time": "2020-04-07 14:09:38"
    },
	"trace_id": "4df57b46fe2af2c3"
}
```

#### 3.2.2. 退款结果通知 

>当退款完成时，连连泰国会推送退款结果至商户退款申请时填入的“notify_url”地址。商户返回时，HTTP code必须为200。

触发频率: 10mins

最大通知次数：13

<b>通知参数列表（连连泰国 至 商户）:</b>

|<div style="width:110px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:338px">Description</div>|
| --- | --- | --- | --- | 
|refund_order_id	|string(20)	|<center>Y</center>	|连连退款单 ID|
|merchant_refund_id	|string(64)	|<center>Y</center>	|商户退款单 ID|
|refund_amount	|string(8,2)	|<center>Y</center>	|退款金额|
|refund_currency	|string (3)	|<center>Y</center>	|币种|
|refund_status	|string (8)	|<center>Y</center>	|退款状态|
|complete_time	|string(19)	|<center>N</center>	|退款完成时间|


<b>响应参数列表（商户 至 连连）:</b>

|<div style="width:130px">Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:338px">Description</div>|
| --- | --- | --- | --- | 
|code|string(6)|<center>Y</center> | 固定: 200000|
|message|string(32)|<center>N</center> | 固定: Success|

<b>请求样例：</b>
```
{
	"refund_order_id": "122020040700160037",
    "merchant_refund_id": "test_refund_022",
    "refund_amount": "100.00",
    "refund_currency": "THB",
    "refund_status": "RP",
    "complete_time": "2020-04-07 14:09:38"
}
```
<b>返回样例:</b>
```
{
	"code": 200000,
	"message": "Success"
}
```

#### 3.2.3. 退款查询 

<b>请求参数列表:</b>

|<div style="width:129px" align="left"> 沙箱 URL </div>|<div style="width:533px" align="left"> https://sandbox-th.lianlianpay-inc.com/gateway </div>| 
| --- | --- | 
| <b>生产 URL</b> |https://api.lianlianpay.co.th/gateway | 
|<b>请求方式 </b> | GET|

|<div style="width:110px">Body Field</div>|<div style="width:90px">Type</div>|<div style="width:50px">Required</div>|<div style="width:345px">Description</div>|
| --- | --- | --- | --- | 
| version | string | <center>Y</center> | 接口版本号，固定值: v1 | 
| service| string |<center>Y</center> | 接口名称，固定值： llpth.bankcard.refund.query| 
| merchant_id | string(20) | <center>Y</center> | 连连泰国提供的商户ID | 
| merchant_order_id |string(64）|	<center>Y</center>|商户退款单号 | 

<b>响应参数列表:</b>

> 通用响应字段 [详见](./#GenResMess)

| Response Parameters: Field  | Type| Required  | <div style="width:304px">Description</div>|
| --- | --- | --- | --- | 
|merchant_id	|string(32)	|<center>Y</center>	|商户ID|
|merchant_refund_id	|string(64)	|<center>Y</center>	|商户退款单ID|
|merchant_order_id	|string(64)	|<center>Y</center>	|商户原支付单 ID|
|order_id	|string(20)	|<center>Y</center>	|原支付单 ID|
|refund_order_id	|string(20)	|<center>Y</center>	|订单 ID|
|refund_status	|string(8)	|<center>M</center>	|退款状态|
|refund_amount	|string(8, 2)	|<center>Y</center>	|退款金额，不能大于支付金额|
|refund_currency	|string(3)	|<center>Y</center>	|退款币种|
|refund_reason	|string(256)	|<center>N</center>	|退款原因|
|notify_url	|string(256)	|<center>N</center>	|退款结果通知地址|
|create_time	|string(19)	|<center>Y</center>	|退款发起时间|
|complete_time	|string(19)	|<center>N</center>	|退款完成时间|


<b>请求样例:</b>
```
https://sandbox-th.lianlianpay-inc.com/gateway?version=v1&service=llpth.bankcard.refund.query&merchant_id=142019050800009001&merchant_refund_id=test_refund_022
```
<b>返回样例:</b>
```
{
	"code": 200000,
	"data": {
		"merchant_id": "142019050800009001",
		"merchant_refund_id": "test_refund_022",
		"merchant_order_id": "test_019",
		"order_id": "122020040700160016",
		"refund_order_id": "122020040700160037",
      	"refund_status": "RS",
		"refund_amount": "100.00",
		"refund_currency": "THB",
		"refund_reason": "refund reason",
		"notify_url": "",        
      	"create_time": "2020-04-07 14:09:38",        
      	"complete_time": "2020-04-07 14:09:40"
    },
    "message": "Success",
    "trace_id": "78a74d643a9a0106"
}
```

## 4. 附录 <!-- {docsify-ignore} -->
> [详见](./#Appendix)