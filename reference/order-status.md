# Order status
|<div style="width:95px">Order status</div>|<div style="width:130px">Description</div>|
| --- | --- |
| PI| Initialized not paid|
| WP| Waiting for Payment|
| PS| Payment Success|
| PF| Payment Failed |
| PE| Payment Expired|
| PC| Payment Cancelled|
