# WooCommerce商户接入Lianlian Pay 引导

**1. 商户号创建**

**第1步:** 提交准入要求资料给我们并完成审核 

**第2步:** 核通过后，请提供接入要求参数给连连:

<ul><ul> ✓ 域名 (店铺地址), e.g https://test.myshop.com/</ul></ul>
<ul><ul> ✓ IP 地址 </ul></ul>
<ul><ul> ✓ 公钥 (RSA格式)</ul></ul>

**第3步:** 连连将配置您的账户并提供以下信息:

<ul><ul> ✓ 商户号</ul></ul>
<ul><ul> ✓ 连连公钥</ul></ul>


**2. 货币设置检查**
目前Lianlian Pay仅支持 THB，请确认WooCommerce账户 “Settings” 中的货币设置为THB
<div align="center">
<img width="700" src="./images/woocommerce/step2.PNG"/>
</div>

**3. 在 WooCommerce店铺中添加插件**
**第1步:** 点击菜单 Plugin > Add New > 通过关键词 “Lianlian”进行查询 > 点击 Install Now

<div align="center">
<img width="700" src="./images/woocommerce/step3-1.PNG"/>
</div>

**第2步:** 点击 Activate 并等待插件激活成功
<div align="center">
<img width="700" src="./images/woocommerce/step3-2.PNG"/>
</div>

**第3步:** 激活成功后，插件列表中将会展示 Lianlian Pay for WooCommerce, 并点击 “Settings”继续
<div align="center">
<img width="700" src="./images/woocommerce/step3-3.PNG"/>
</div>

**第4步:** 如果您想在沙箱环境中进行测试，可勾选 Enable Lianlian sandbox 

<ul><ul> ✓ 输入 Merchant ID, Public Key (由Lianlian Pay提供) </ul></ul>
<ul><ul> ✓ 输入私钥 (由商户生成)  </ul></ul>
<ul><ul> ✓ 点击 “Save Changes”进行保存 </ul></ul>

<div align="center">
<img width="700" src="./images/woocommerce/step4.PNG"/>
</div>

**第5步:** 前往WooCommerce > Setting > Payment ，开启Lianlian Credit/Debit Payment
<div align="center">
<img width="700" src="./images/woocommerce/step5.PNG"/>
</div>