# Shopify商户接入Lianlian Pay引导

<b>第1步</b>: 提交准入要求资料和接入要求参数给我们 
<ul><ul> ✓ 域名 (店铺地址), e.g https://test.myshopify.com/</ul></ul>
<ul><ul> ✓ IP whitelist </ul></ul>
<b>第2步</b>: 完成准入审核

<b>第3步</b>: 审核通过后, <u><b>Merchant ID</b></u> 和 <u><b>Secret key</b></u> 将会发送到您的邮箱

<b>第4步</b>: 店铺货币检查.<br> 目前连连仅支持收单货币为THB，在您开启支付设置之前，请确保您的店铺货币已经设置为THB。

<div align="center">
<img width="800" src="./images/shopify/step4.PNG"/>
</div>

<b>第5步</b>: Shopify配置
<ul><ul> ✓ 在Settings页下，点击 “Payments” 并继续</ul></ul>
<div align="center">
<img width="800" src="./images/shopify/step5.PNG"/>
</div>

<ul><ul> ✓ 在支付提供商下, 点击 “Choose third-party provider””</ul></ul>
<div align="center">
<img width="800" src="./images/shopify/step5.1.PNG"/>
</div>

<ul><ul> ✓ 选择Lianlian Pay 并继续</ul></ul>
<div align="center">
<img width="800" src="./images/shopify/step5.2.PNG"/>
</div>

<ul><ul> ✓ 输入您在邮件中收到的 <u><b>Merchant ID</b></u> 和 <u><b>Secret key</b></u></ul></ul>
<div align="center">
<img width="800" src="./images/shopify/step5.3.PNG"/>
</div>

<ul><ul> ✓ 选择 <u><b>Visa, Mastercard, JCB</b></u> 作为支付方式并提交。(<u>说明: American Express, Discover, and Diners Club暂不支持</u>) </ul></ul>
<div align="center">
<img width="800" src="./images/shopify/step5.4.PNG"/>
</div>

<ul><ul> ✓ 配置成功! Lianlian Pay 已成功激活</ul></ul>
<div align="center">
<img width="800" src="./images/shopify/step5.5.PNG"/>
</div>